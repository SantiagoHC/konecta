<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Products;
use App\References;
use App\Sales;


class CategoriesController extends Controller
{
    /** funcion para listar todos los datos reuqeridos en esta vista
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function ListProducts()
    {
        /* Cosultamos todas las categorias activas en bd */
        $Categories = Categories::
        select(
        'IDCategory',
        'Name',
        'Description',
        'Status'
        )
        ->where('Status', 1)
        ->orderBy('IDCategory', 'desc')->get();

        return view('Categories', compact(['Categories']));
    }
    /** funcion para crear una categoria
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function CreateCategories(Request $request)
    {
        /* Validamos que los datos requeridos para la inserccion existan en base de datos */
        $validate = \Validator::make($request->all(), [
            'Name'            => 'required'
        ]);
        if ($validate->fails()) {
            $info = $request->session()->flash('failed','Porfavor envia todos los datos requeridos');
            return redirect('Categories')->with('info', $info);
        } else {
            /* Obtenemos todos los datos enviados por el formulario */
            $data = $request->input();
			try{
                /* Validamos si la descripcion viene vacia y en caso tal guardala como null */
                if (!empty($data['Description'])) {
                    $Description = $data['Description'];
                }else {
                    $Description = NULL;
                }
                /* Almacenamos en bd los datos requeridos en Bd */
                $Categories = new Categories;
                $Categories->Name = $data['Name'];
                $Categories->Description = $Description;
                $Categories->Status = 1;
                /* Si guarda notificar que fue correcto el ingreso */
                if ($Categories->save()) {
                    $info = $request->session()->flash('successMsg','Se creo exitosamente la categoria '.$data['Name']);
                    return redirect('Categories')->with('info', $info);
                }else {
                    $info = $request->session()->flash('failed','Se pudo guardar la categoria '.$data['Name']);
                    return redirect('Categories')->with('info', $info);
                }
			}
			catch(Exception $e){
                $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
                return redirect('Categories')->with('info', $info);
			}
		} 
    }
    /** funcion para editar una categoria
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function EditCategories($IDCategory, Request $request)
    {
        /* Validamos que los datos requeridos para la inserccion existan en base de datos */
        $validate = \Validator::make($request->all(), [
            'Name'            => 'required'
        ]);
        if ($validate->fails()) {
            $info = $request->session()->flash('failed','Porfavor envia todos los datos requeridos');
            return redirect('Categories')->with('info', $info);
        } else {
            /* Obtenemos todos los datos enviados por el formulario */
            $data = $request->input();
			try{
                /* Validamos si la descripcion viene vaciay en caso tal guardala como null */
                if (!empty($data['Description'])) {
                    $Description = $data['Description'];
                }else {
                    $Description = NULL;
                }
                /* actualizamos todos los datos recibidos a la tabla de categorias */
                $Categories = Categories::where('IDCategory',$IDCategory)
                ->update(
                    array(
                        'Name' => $data['Name'],
                        'Description' => $Description
                    )
                );
                /* Si guarda notificar que fue correcta modificacion */
                if ($Categories) {
                    $info = $request->session()->flash('successMsg','Se edito exitosamente la categoria '.$data['Name']);
                    return redirect('Categories')->with('info', $info);
                }else {
                    $info = $request->session()->flash('failed','No se pudo editar la categoria '.$data['Name']);
                    return redirect('Categories')->with('info', $info);
                }
			}
			catch(Exception $e){
                $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
                return redirect('Categories')->with('info', $info);
			}
		}   
    }
    /** funcion para inactivar una categoria
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function InactivateCategories($IDCategory, Request $request)
    {
        try{
            /* Actualizamos el estado de una categoria segun el id recibido */
            $Categories = Categories::where('IDCategory',$IDCategory)
            ->update(
                array(
                    'Status' => 0,
                )
            );
            /* si actualiza correctamente notificar */
            if ($Categories) {
                $info = $request->session()->flash('successMsg','Se elimino exitosamente la categoria');
                return redirect('Categories')->with('info', $info);
            }else {
                $info = $request->session()->flash('failed','No se pudo eliminar la categoria');
                    return redirect('Categories')->with('info', $info);
            }
        }
        catch(Exception $e){
            $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
            return redirect('Categories')->with('info', $info);
        }
    }


}
