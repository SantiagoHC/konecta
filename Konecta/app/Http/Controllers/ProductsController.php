<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Products;
use App\References;
use App\Sales;

class ProductsController extends Controller
{
     /** funcion para listar todos los datos reuqeridos en esta vista
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function ListProducts()
    {
        /* consultamos todas los productos activos*/
        $Products = Products::
        join('categories as c' , 'c.IDCategory', '=' ,'Products.IDCategory')
        ->join('references as r' , 'r.IdReference', '=' ,'Products.IdReference')
        ->select(
        'Products.Id',
        'Products.Product',
        'r.Name as references',
        'r.IdReference as IdReference',
        'c.IDCategory as IDCategory',
        'c.Name as categories',
        'Products.Price',
        'Products.Weight',
        'Products.stock',
        'Products.CreationDate'
        )
        ->where('Products.Status', 1)
        ->orderBy('Products.Id', 'desc')->get();

        /* consultamos todas las categorias activas */
        $Categories = Categories::
        select(
        'IDCategory',
        'Name',
        'Description',
        'status'
        )
        ->where('Status', 1)
        ->orderBy('IDCategory', 'desc')->get();

        /* consultamos todas las referencias activas */
        $References = References::
        select(
        'IdReference',
        'Name',
        'Description',
        'status'
        )
        ->where('Status', 1)
        ->orderBy('IdReference', 'desc')->get();

        return view('Product', compact(['Products','Categories','References']));
    }
     /** funcion para crear productos
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function CreateProducts(Request $request){
        /* Validamos que los datos requeridos para la inserccion existan en base de datos */
        $validate = \Validator::make($request->all(), [
            'Product'            => 'required',
            'IdReference'        => 'required',
            'IDCategory'         => 'required',
            'Price'              => 'required',
            'Weight'             => 'required',
            'Stock'              => 'required'
        ]);
        if ($validate->fails()) {
            $info = $request->session()->flash('failed','Porfavor envia todos los datos requeridos');
            return redirect('product')->with('info', $info);
        } else {
            /* Obtenemos todos los datos enviados por el formulario */
            $data = $request->input();
			try{
                /* traemos la fecha */
                date_default_timezone_set("America/Bogota");
                $CreateDate = date('Y-m-d H:i:s');
                /* Almacenamos en bd los datos requeridos en Bd */
                $Products = new Products;
                $Products->Product = $data['Product'];
                $Products->IdReference = $data['IdReference'];
                $Products->IDCategory = $data['IDCategory'];
                $Products->Price = $data['Price'];
                $Products->Weight = $data['Weight'];
                $Products->Stock     = $data['Stock'];
                $Products->CreationDate = $CreateDate;
                /* Si guarda notificar que fue correcto el ingreso */
                if ($Products->save()) {
                    $info = $request->session()->flash('successMsg','Se creo exitosamente el producto '.$data['Product']);
                    return redirect('product')->with('info', $info);
                }else {
                    $info = $request->session()->flash('failed','Se pudo guardar el producto '.$data['Product']);
                    return redirect('product')->with('info', $info);
                }
			}
			catch(Exception $e){
                $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
                return redirect('product')->with('info', $info);
			}
		} 
    }
     /** funcion para editar productos
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function EditProducts($IdProduct, Request $request)
    {
        /* Validamos que los datos requeridos para la inserccion existan en base de datos */
        $validate = \Validator::make($request->all(), [
            'Product'            => 'required',
            'IdReference'            => 'required',
            'IDCategory'            => 'required',
            'Weight'            => 'required',
            'Stock'            => 'required'
        ]);
        if ($validate->fails()) {
            $info = $request->session()->flash('failed','Porfavor envia todos los datos requeridos');
            return redirect('product')->with('info', $info);
        } else {
            /* Obtenemos todos los datos enviados por el formulario */
            $data = $request->input();
			try{
                /* traemos la fecha */
                date_default_timezone_set("America/Bogota");
                $CreateDate = date('Y-m-d H:i:s');
                /* Actualizamos en base de datos el producto editado */
                $Product = Products::where('Id',$IdProduct)
                ->update(
                    array(
                        'Product' => $data['Product'],
                        'IdReference'=> $data['IdReference'],
                        'IDCategory' => $data['IDCategory'],
                        'Weight' => $data['Weight'],
                        'Stock'     => $data['Stock'],
                        'CreationDate' => $CreateDate,
                    )
                );
                /* Si guarda notificar que fue correcto el ingreso */
                if ($Product) {
                    $info = $request->session()->flash('successMsg','Se edito exitosamente el producto '.$data['Product']);
                    return redirect('product')->with('info', $info);
                }else {
                    $info = $request->session()->flash('failed','Se pudo editar el producto '.$data['Product']);
                    return redirect('product')->with('info', $info);
                }
			}
			catch(Exception $e){
                $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
                return redirect('product')->with('info', $info);
			}
		} 
    }
     /** funcion para inactivar productos
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function InactivateProducts($IdProduct, Request $request)
    {
        try{
            /* Actualizamos el estado de un producto segun el id recibido */
            $Product = Products::where('Id',$IdProduct)
            ->update(
                array(
                    'Status' => 0,
                )
            );
            /* si actualiza correctamente notificar */
            if ($Product) {
                $info = $request->session()->flash('successMsg','Se elimino exitosamente el producto');
                return redirect('product')->with('info', $info);
            }else {
                $info = $request->session()->flash('failed','Se pudo eliminar el producto');
                return redirect('product')->with('info', $info);
            }
        }
        catch(Exception $e){
            $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
            return redirect('product')->with('info', $info);
        }
    }


}
