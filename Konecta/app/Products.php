<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    const UPDATED_AT = null;
    const CREATED_AT = null;
    protected $table = 'products';
}
