<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

        <link rel="stylesheet" href="{{ asset('css/Product.css') }}" />

        <title>Ventas</title>
    </head>
    <body>
        <input type="checkbox" id="nav-toggle" />
        <div class="sidebar">
            <div class="sidebar-brand">
                <h2>
                    <span class="lab la-Konecta"></span>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFLBnjrbBBbCTG3alOhgryEFg_AZ-dySnuKw&usqp=CAU" class="logosidebar" width="50%" alt="" />
                </h2>
            </div>
            <div class="sidebar-menu">
                <ul>
                    <li>
                        <a href="{{ url('/') }}"><span class="bi bi-receipt"></span> <span>Ventas</span></a>
                    </li>
                    <li>
                        <a href="{{ url('product') }}"><span class="bi bi-cart"></span> <span>Productos</span></a>
                    </li>
                    <li>
                        <a href="{{ url('Categories') }}"><span class="bi bi-columns"></span> <span>Categorias</span></a>
                    </li>
                    <li>
                        <a href="{{ url('References') }}"><span class="bi bi-collection"></span> <span>Referencias</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-content">
            <header>
                <h2>
                    <label for="nav-toggle">
                        <i class="fa fa-align-justify" aria-hidden="true"></i>
                    </label>
                    Ventas
                </h2>

                <div class="user-wrapper">
                    <div *ngIf="identity">
                        <img src="https://resources.bloo.media/hubfs/pau%20ordu%C3%B1a.jpg" width="40px" height="40px" alt="" />
                    </div>
                    <div>
                        <small>SuperAdmin</small>
                        <h4>Santiago Hernandez</h4>
                    </div>
                </div>
            </header>
            <main>
            @if(Session::has('successMsg'))
                <div class="alert alert-success"> {{ Session::get('successMsg') }}</div>
            @endif
            @if(Session::has('failed'))
            <div class="alert alert-danger"> {{ Session::get('failed')  }}</div>
            @endif
            <div class="card-deck">
                <div class="card">
                    <i class="bi bi-arrow-up-circle" ></i>
                    <div class="card-body">
                    <h5 class="card-title">El producto con mas cantidad </h5>
                    <p class="card-text">{{$TopStockProducts->Product}} con {{$TopStockProducts->Stock}} unidades</p>
                    </div>
                </div>
                <div class="card">
                    <i class="bi bi-bar-chart-fill"></i>
                    <div class="card-body">
                    <h5 class="card-title">El producto mas vendido</h5>
                    <p class="card-text">{{$TopSales->Product}}</p>
                    </div>
                </div>
                </div>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#CreateSales">
                    Crear Venta
                </button>
                <div class="modal fade" id="CreateSales" tabindex="-1" role="dialog" aria-labelledby="CreateSales" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Crear Venta</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="create" action="/KonectaC/Konecta/public/Createsale" method="post">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                    <table>
                                        <tr>
                                            <td>Producto</td>
                                            <td>
                                                <select name="ProductId">
                                                    @foreach ($Products as $Product)
                                                    <option value="{{$Product->Id}}">{{$Product->Product}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cantidad</td>
                                            <td><input type="number" name="ProductStock" /></td>
                                        </tr>

                                        <tr>
                                            <td colspan="2">
                                                <input class="btn btn-success" type="submit" value="Agregar" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Valor</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($Sales as $Sale)
                        <tr>
                            <th>{{$Sale->IdSales}}</th>
                            <td>{{$Sale->Product}}</td>
                            <td>{{$Sale->Stock}}</td>
                            <td>{{$Sale->PaymentPrice}}</td>
                            <td>{{$Sale->CreateDate}}</td>
                            <td>
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#EditSales{{$Sale->IdSales}}">
                                    Editar
                                </button>
                                <div class="modal fade" id="EditSales{{$Sale->IdSales}}" tabindex="-1" role="dialog" aria-labelledby="EditSales{{$Sale->IdSales}}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Editar Venta</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form id="edit" action="/KonectaC/Konecta/public/editsale/{{$Sale->IdSales}}" method="post">
                                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
                                                    <table>
                                                        <tr>
                                                            <td>Producto</td>
                                                            <td>
                                                                <select name="ProductId">
                                                                    <option value="{{$Sale->ProductId}}">{{$Sale->Product}}</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Cantidad</td>
                                                            <td><input type="number" name="ProductStock" placeholder="{{$Sale->Stock}}" /></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <input class="btn btn-success" type="submit" value="Editar" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </main>
        </div>
    </body>
</html>
