<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

        <link rel="stylesheet" href="{{ asset('css/Product.css') }}" />

        <title>Productos</title>
    </head>
    <body>
        <input type="checkbox" id="nav-toggle" />
        <div class="sidebar">
            <div class="sidebar-brand">
                <h2>
                    <span class="lab la-Konecta"></span>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFLBnjrbBBbCTG3alOhgryEFg_AZ-dySnuKw&usqp=CAU" class="logosidebar" width="50%" alt="" />
                </h2>
            </div>
            <div class="sidebar-menu">
                <ul>
                    <li>
                        <a href="{{ url('/') }}"><span class="bi bi-receipt"></span> <span>Ventas</span></a>
                    </li>
                    <li>
                        <a href="{{ url('product') }}"><span class="bi bi-cart"></span> <span>Productos</span></a>
                    </li>
                    <li>
                        <a href="{{ url('Categories') }}"><span class="bi bi-columns"></span> <span>Categorias</span></a>
                    </li>
                    <li>
                        <a href="{{ url('References') }}"><span class="bi bi-collection"></span> <span>Referencias</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-content">
            <header>
                <h2>
                    <label for="nav-toggle">
                        <i class="fa fa-align-justify" aria-hidden="true"></i>
                    </label>
                    Productos
                </h2>

                <div class="user-wrapper">
                    <div *ngIf="identity">
                        <img src="https://resources.bloo.media/hubfs/pau%20ordu%C3%B1a.jpg" width="40px" height="40px" alt="" />
                    </div>
                    <div>
                        <small>SuperAdmin</small>
                        <h4>Santiago Hernandez</h4>
                    </div>
                </div>
            </header>
            <main>
                @if(Session::has('successMsg'))
                    <div class="alert alert-success"> {{ Session::get('successMsg') }}</div>
                @endif
                @if(Session::has('failed'))
                <div class="alert alert-danger"> {{ Session::get('failed')  }}</div>
                @endif
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#CreateProduct">
                    Crear Productos
                </button>
                <div class="modal fade" id="CreateProduct" tabindex="-1" role="dialog" aria-labelledby="CreateProduct" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Crear Productos</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="/KonectaC/Konecta/public/createproduct" method="post">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                    <table>
                                        <tr>
                                            <td>Producto</td>
                                            <td><input type="text" name="Product" /></td>
                                        </tr>
                                        <tr>
                                            <td>Referencia</td>
                                            <td>
                                                <select name="IdReference">
                                                    @foreach ($References as $Reference)
                                                    <option value="{{$Reference->IdReference}}">{{$Reference->Name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Categoria</td>
                                            <td>
                                                <select name="IDCategory">
                                                    @foreach ($Categories as $Category)
                                                    <option value="{{$Category->IDCategory}}">{{$Category->Name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Precio</td>
                                            <td><input type="number" pattern="^[0-9]" min="1" step="1" name="Price" /></td>
                                        </tr>
                                        <tr>
                                            <td>Peso</td>
                                            <td>
                                                <input type="number" pattern="^[0-9]" min="1" step="1" name="Weight" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cantidad</td>
                                            <td>
                                                <input type="number" pattern="^[0-9]" min="1" step="1" name="Stock" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <input class="btn btn-success" type="submit" value="Agregar" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Referencia</th>
                            <th scope="col">Categoría</th>
                            <th scope="col">Peso</th>
                            <th scope="col">Stock</th>
                            <th scope="col">Fecha de creación</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($Products as $Product)
                        <tr>
                            <th>{{$Product->Id}}</th>
                            <td>{{$Product->Product}}</td>
                            <td>{{$Product->references}}</td>
                            <td>{{$Product->categories}}</td>
                            <td>{{$Product->Weight}}</td>
                            <td>{{$Product->stock}}</td>
                            <td>{{$Product->CreationDate}}</td>
                            <th>${{$Product->Price}}</th>
                            <th>
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#Editproduct{{$Product->Id}}">
                                    Editar
                                </button>
                                <div class="modal fade" id="Editproduct{{$Product->Id}}" tabindex="-1" role="dialog" aria-labelledby="Editproduct{{$Product->Id}}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Editar Producto</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form id="edit" action="/KonectaC/Konecta/public/editproduct/{{$Product->Id}}" method="post">
                                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                                    <table>
                                                        <tr>
                                                            <td>Producto</td>
                                                            <td><input type="text" name="Product" value="{{$Product->Product}}"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Referencia</td>
                                                            <td>
                                                                <select name="IdReference">
                                                                    <option value="{{$Product->IdReference}}">{{$Product->references}}</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Categoria</td>
                                                            <td>
                                                                <select name="IDCategory">
                                                                    <option value="{{$Product->IDCategory}}">{{$Product->categories}}</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Precio</td>
                                                            <td><input type="number" pattern="^[0-9]" min="1" step="1" name="Price" value="{{$Product->Price}}" disabled="disabled"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Peso</td>
                                                            <td>
                                                                <input type="number" pattern="^[0-9]" min="1" step="1" name="Weight" value="{{$Product->Weight}}"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Cantidad</td>
                                                            <td>
                                                                <input type="number" pattern="^[0-9]" min="1" step="1" name="Stock" value="{{$Product->stock}}" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <input class="btn btn-success" type="submit" value="Editar" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#inactivateproduct{{$Product->Id}}">
                                    Inactivar
                                </button>
                                <div class="modal fade" id="inactivateproduct{{$Product->Id}}" tabindex="-1" role="dialog" aria-labelledby="inactivateproduct{{$Product->Id}}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Inactivar Producto</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form id="edit" action="/KonectaC/Konecta/public/inactivateproduct/{{$Product->Id}}" method="post">
                                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                                    <table>
                                                        <tr>
                                                            <p>¿Seguro deseas eliminar el producto {{$Product->Product}}?</p>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <input class="btn btn-success" type="submit" value="Aceptar" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </main>
        </div>
    </body>
</html>
