<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Categories;
use App\Products;
use App\References;
use App\Sales;

class SalesController extends Controller
{
     /** funcion para listar todos los datos reuqeridos en esta vista
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function ListProducts(){
        /* consultamos todas las ventas realizadas */
        $Sales = Sales::
        join('products as p' , 'p.Id', '=' ,'Sales.IdProduct')
        ->select(
        'Sales.IdSales',
        'p.Product',
        'p.Id as ProductId',
        'Sales.Stock',
        'Sales.PaymentPrice',
        'CreateDate'
        )
        ->orderBy('Sales.IdSales', 'desc')->get();

        /* Consultamos todos los productos activos */
        $Products = Products::
        join('categories as c' , 'c.IDCategory', '=' ,'Products.IDCategory')
        ->join('references as r' , 'r.IdReference', '=' ,'Products.IdReference')
        ->select(
        'Products.Id',
        'Products.Product'
        )
        ->where('Products.Status', 1)
        ->orderBy('Products.Id', 'desc')->get();

        $TopStockProducts = Products::
        select(
        'Products.Id',
        'Products.Product',
        'Products.Stock'
        )
        ->where('Products.Status', 1)
        ->orderBy('Products.Stock', 'desc')
        ->first();

        $TopSales = Sales::
        join('products as p' , 'p.Id', '=' ,'Sales.IdProduct')
        ->select(
            DB::raw("COUNT( Sales.IdProduct ) AS total"),
            'p.Product'
            )
            ->where('p.Status', 1)
            ->groupBy('p.Product')
            ->orderBy('total', 'desc')
            ->first();

        return view('Sales', compact(['Sales','Products','TopStockProducts','TopSales']));
    }
     /** funcion para crear ventas
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function CreateSales(Request $request)
    {
        /* Validamos que los datos requeridos para la inserccion existan en base de datos */
        $validate = \Validator::make($request->all(), [
            'ProductId'            => 'required',
            'ProductStock'         => 'required',
        ]);
        if ($validate->fails()) {
            $info = $request->session()->flash('failed','Porfavor envia todos los datos requeridos');
            return redirect('/')->with('info', $info);
        } else {
            /* Obtenemos todos los datos enviados por el formulario */
            $data = $request->input();
			try{
                /* Cosultamos los productos que tenemos segun el id enviado */
                $Products = Products::
                join('categories as c' , 'c.IDCategory', '=' ,'Products.IDCategory')
                ->join('references as r' , 'r.IdReference', '=' ,'Products.IdReference')
                ->select(
                'Products.Id',
                'Products.Product',
                'Products.Price',
                'Products.Stock'
                )
                ->where('Products.Id',$data['ProductId'])
                ->first();
                /* Si encontramos datos o la cantidad de productos es menor a la disponible entra a la condicon */
                if (is_object($Products) && $Products->Stock>0 && $data['ProductStock'] <= $Products->Stock) {
                    /* Actualizamos los productos segun los productos enviados */
                    Products::where('Products.Id',$data['ProductId'])
                    ->update(
                        array(
                            'Stock' => $Products->Stock - $data['ProductStock']
                        )
                    );
                    /* traemos la fecha */
                    date_default_timezone_set("America/Bogota");
                    $CreateDate = date('Y-m-d H:i:s');

                    /* Almacenamos en bd los datos requeridos en Bd */
                    $Sales = new Sales;
                    $Sales->IdProduct = $data['ProductId'];
                    $Sales->Stock = $data['ProductStock'];
                    $Sales->PaymentPrice = $Products->Price *  $data['ProductStock'];
                    $Sales->CreateDate =$CreateDate;
                    /* Si guarda notificar que fue correcto el ingreso */
                    if ($Sales->save()) {
                        $info = $request->session()->flash(
                            'successMsg','Se creo corretamente la venta de '.$data['ProductStock']." ".$Products->Product." con un total de ".$Products->Price *  $data['ProductStock']
                        );
                        return redirect('/')->with('info', $info);
                    }else {
                        $info = $request->session()->flash('failed','No se pudo guardar la venta');
                        return redirect('/')->with('info', $info);
                    }
                }else{
                    $info = $request->session()->flash('failed','Error, El producto no cuenta con la cantidad de '.$data['ProductStock']);
                    return redirect('/')->with('info', $info);
                }
			}
			catch(Exception $e){
                $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
                return redirect('/')->with('info', $info);
			}
		} 
    }
    /** funcion para editar ventas
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function EditSales($IdSales, Request $request)
    {
        /* Validamos que los datos requeridos para la inserccion existan en base de datos */
        $validate = \Validator::make($request->all(), [
            'ProductId'            => 'required',
            'ProductStock'         => 'required',
        ]);
        if ($validate->fails()) {
            $info = $request->session()->flash('failed','Porfavor envia todos los datos requeridos');
            return redirect('/')->with('info', $info);
        } else {
            /* Obtenemos todos los datos enviados por el formulario */
            $data = $request->input();
			try{
                /* Cosultamos los productos que tenemos segun el id enviado */
                $Products = Products::
                join('categories as c' , 'c.IDCategory', '=' ,'Products.IDCategory')
                ->join('references as r' , 'r.IdReference', '=' ,'Products.IdReference')
                ->select(
                'Products.Id',
                'Products.Price',
                'Products.Stock'
                )
                ->where('Products.Id',$data['ProductId'])
                ->first();
                /* Si encontramos datos o la cantidad de productos es menor a la disponible entra a la condicon */
                if (is_object($Products) && $Products->Stock>0 &&  $data['ProductStock'] <= $Products->Stock) {
                    /* Actualizamos los productos segun los productos enviados */
                    Products::where('Products.Id',$data['ProductId'])
                    ->update(
                        array(
                            'Stock' => $Products->Stock - $data['ProductStock']
                        )
                    );
                    /* consultamos cuantos productos tiene la venta para sumar cuantos vamos a ingresar */
                    $Stock = Sales::
                    select(
                    'Stock'
                    )
                    ->where('IdSales',$IdSales)
                    ->first();
                    /* traemos la fecha */
                    date_default_timezone_set("America/Bogota");
                    $CreateDate = date('Y-m-d H:i:s');
                    $AllStock = $Stock->Stock + $data['ProductStock'];
                    /* Almacenamos en bd los datos requeridos en Bd */
                    $Sales = Sales::where('IdSales',$IdSales)
                    ->update(
                        array(
                            'IdProduct' => $data['ProductId'],
                            'Stock' =>$AllStock,
                            'PaymentPrice' =>$AllStock * $Products->Price,
                            'CreateDate' =>$CreateDate
                        )
                    );
                    /* Si guarda notificar que fue correcto la edicion */
                    if ($Sales) {
                        $info = $request->session()->flash(
                            'successMsg','Se creo edito la venta de '.$data['ProductStock']." ".$Products->Product." con un total de ".$AllStock * $Products->Price
                        );
                        return redirect('/')->with('info', $info);
                    }else {
                        $info = $request->session()->flash('failed','No se edito la venta');
                        return redirect('/')->with('info', $info);
                    }
                }else{
                    $info = $request->session()->flash('failed','Error, El producto no cuenta con la cantidad de '.$data['ProductStock']);
                    return redirect('/')->with('info', $info);
                }
			}
			catch(Exception $e){
                $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
                return redirect('/')->with('info', $info);
			}
		} 
    }
}
