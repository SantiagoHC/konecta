-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.36-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para konectac
CREATE DATABASE IF NOT EXISTS `konectac` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `konectac`;

-- Volcando estructura para tabla konectac.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `IDCategory` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDCategory`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla konectac.categories: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`IDCategory`, `Name`, `Description`, `Status`) VALUES
	(1, 'Panaderia', NULL, 1),
	(2, 'Bebidas', NULL, 1),
	(3, 'Pepsi', NULL, 1),
	(4, 'arepa', NULL, 1),
	(5, 'Buñuelo', NULL, 0),
	(6, 'Queso', 'nunguna', 0),
	(7, 'ArepaHuevo', NULL, 0),
	(8, 'Queso', 'nunguna', 0),
	(9, 'Queso', '1', 0),
	(10, 'Queso', 'nunguna2', 0);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Volcando estructura para tabla konectac.products
CREATE TABLE IF NOT EXISTS `products` (
  `Id` int(15) NOT NULL AUTO_INCREMENT,
  `Product` varchar(100) NOT NULL DEFAULT '',
  `IdReference` int(11) NOT NULL,
  `IDCategory` int(11) NOT NULL,
  `Price` int(30) NOT NULL,
  `Weight` varchar(30) NOT NULL,
  `Stock` int(15) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  `CreationDate` datetime NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `FKIdReference` (`IdReference`) USING BTREE,
  KEY `FKIDCategory` (`IDCategory`) USING BTREE,
  CONSTRAINT `FKIDCategory` FOREIGN KEY (`IDCategory`) REFERENCES `categories` (`IDCategory`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKIdReference` FOREIGN KEY (`IdReference`) REFERENCES `references` (`IdReference`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla konectac.products: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`Id`, `Product`, `IdReference`, `IDCategory`, `Price`, `Weight`, `Stock`, `Status`, `CreationDate`) VALUES
	(1, 'santiago', 3, 5, 1200, '33', 42, 1, '2022-02-11 10:26:27'),
	(2, 'arepas', 2, 3, 1200, '33', 18, 1, '2022-02-11 10:26:43'),
	(3, 'santiago', 3, 4, 1200, '99', 99, 0, '2022-02-11 11:00:51');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Volcando estructura para tabla konectac.references
CREATE TABLE IF NOT EXISTS `references` (
  `IdReference` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IdReference`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla konectac.references: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `references` DISABLE KEYS */;
INSERT INTO `references` (`IdReference`, `Name`, `Description`, `Status`) VALUES
	(1, 'bimbo', '3', 1),
	(2, 'ColCafe', '2', 1),
	(3, 'Casero', NULL, 1),
	(4, 'Postobon', NULL, 0);
/*!40000 ALTER TABLE `references` ENABLE KEYS */;

-- Volcando estructura para tabla konectac.sales
CREATE TABLE IF NOT EXISTS `sales` (
  `IdSales` int(11) NOT NULL AUTO_INCREMENT,
  `IdProduct` int(15) NOT NULL,
  `Stock` int(15) NOT NULL,
  `PaymentPrice` int(15) NOT NULL,
  `CreateDate` datetime NOT NULL,
  PRIMARY KEY (`IdSales`) USING BTREE,
  KEY `FKIdProduct` (`IdProduct`) USING BTREE,
  CONSTRAINT `FKIdProduct` FOREIGN KEY (`IdProduct`) REFERENCES `products` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla konectac.sales: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` (`IdSales`, `IdProduct`, `Stock`, `PaymentPrice`, `CreateDate`) VALUES
	(1, 2, 9, 10800, '2022-02-11 10:27:34'),
	(2, 1, 3, 3600, '2022-02-11 11:01:19'),
	(3, 2, 2, 2400, '2022-02-11 11:28:14'),
	(4, 1, 4, 4800, '2022-02-11 11:28:25'),
	(5, 1, 1, 1200, '2022-02-11 11:29:38'),
	(6, 2, 4, 4800, '2022-02-11 11:29:45');
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
