<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
        <link rel="stylesheet" href="{{ asset('css/Product.css') }}" />

        <title>Categorias</title>
    </head>
    <body>
        <input type="checkbox" id="nav-toggle" />
        <div class="sidebar">
            <div class="sidebar-brand">
                <h2>
                    <span class="lab la-Konecta"></span>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFLBnjrbBBbCTG3alOhgryEFg_AZ-dySnuKw&usqp=CAU" class="logosidebar" width="50%" alt="" />
                </h2>
            </div>
            <div class="sidebar-menu">
                <ul>
                    <li>
                        <a href="{{ url('/') }}"><span class="bi bi-receipt"></span> <span>Ventas</span></a>
                    </li>
                    <li>
                        <a href="{{ url('product') }}"><span class="bi bi-cart"></span> <span>Productos</span></a>
                    </li>
                    <li>
                        <a href="{{ url('Categories') }}"><span class="bi bi-columns"></span> <span>Categorias</span></a>
                    </li>
                    <li>
                        <a href="{{ url('References') }}"><span class="bi bi-collection"></span> <span>Referencias</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-content">
            <header>
                <h2>
                    <label for="nav-toggle">
                        <i class="fa fa-align-justify" aria-hidden="true"></i>
                    </label>
                    Categorias
                </h2>

                <div class="user-wrapper">
                    <div *ngIf="identity">
                        <img src="https://resources.bloo.media/hubfs/pau%20ordu%C3%B1a.jpg" width="40px" height="40px" alt="" />
                    </div>
                    <div>
                        <small>SuperAdmin</small>
                        <h4>Santiago Hernandez</h4>
                    </div>
                </div>
            </header>
            <main>
                 @if(Session::has('successMsg'))
                    <div class="alert alert-success"> {{ Session::get('successMsg') }}</div>
                @endif
                @if(Session::has('failed'))
                <div class="alert alert-danger"> {{ Session::get('failed')  }}</div>
                @endif
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#CreateCategories">
                    Crear Categorias
                </button>

                <div class="modal fade" id="CreateCategories" tabindex="-1" role="dialog" aria-labelledby="CreateCategories" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Crear Categorias</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="/KonectaC/Konecta/public/createcategory" method="post">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                    <table>
                                        <tr>
                                            <td>Nombre</td>
                                            <td><input type="text" name="Name" /></td>
                                        </tr>
                                        <tr>
                                            <td>Descripcion</td>
                                            <td><input type="text" name="Description" /></td>
                                        </tr>

                                        <tr>
                                            <td colspan="2">
                                                <input class="btn btn-success" type="submit" value="Agregar" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Descripcion</th>
                            <th>Acciones</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($Categories as $Category)
                        <tr>
                            <th>{{$Category->IDCategory}}</th>
                            <td>{{$Category->Name}}</td>
                            <td>{{$Category->Description}}</td>
                            <td>
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#EditCategories{{$Category->IDCategory}}">
                                    Editar
                                </button>
                                <div class="modal fade" id="EditCategories{{$Category->IDCategory}}" tabindex="-1" role="dialog" aria-labelledby="EditCategories{{$Category->IDCategory}}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Editar Categoria</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form id="edit" action="/KonectaC/Konecta/public/editcategories/{{$Category->IDCategory}}" method="post">
                                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
                                                    <table>
                                                        <tr>
                                                            <td>Nombre</td>
                                                            <td>
                                                                <select name="Name">
                                                                    <option value="{{$Category->Name}}">{{$Category->Name}}</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Descripcion</td>
                                                            <td><input type="text" name="Description" placeholder="{{$Category->Description}}" /></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <input class="btn btn-success" type="submit" value="Editar" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#inactivatecategory{{$Category->IDCategory}}">
                                    Inactivar
                                </button>
                                <div class="modal fade" id="inactivatecategory{{$Category->IDCategory}}" tabindex="-1" role="dialog" aria-labelledby="inactivatecategory{{$Category->IDCategory}}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Inactivar categoria</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form id="edit" action="/KonectaC/Konecta/public/inactivatecategory/{{$Category->IDCategory}}" method="post">
                                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
                                                    <table>
                                                        <tr>
                                                            <p>¿Seguro deseas eliminar la categoria {{$Category->Name}}?</p>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <input class="btn btn-success" type="submit" value="Aceptar" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </main>
        </div>
    </body>
</html>
