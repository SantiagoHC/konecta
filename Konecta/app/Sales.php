<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    const UPDATED_AT = null;
    const CREATED_AT = null;
    protected $table = 'sales';
}
