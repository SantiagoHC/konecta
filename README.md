Proyecto 100% en laravel,

El proyecto debe tener el siguiente encarpetado:
KonectaC/Konecta/…

Para ejecutar el proyecto se debe de agregar un achivo llamado .env el la ruta principal y debe contener lo siguiente:

---------------------------------------------------------------------------------------------------------------------------------------
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:KfvXJcMB1k5/ErlwfxKxyWbZOS4hZxJ5xN6DdYCSrko=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=konectac
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

-----------------------------------------------------------------------------------------------------------------------------------------

luego se debe crear se de dirigir a la carpeta raiz desde el simbolo de sistema:

* windows + r
* cmd
* se debe ingresar lo siguiente 
* cd (Ruta del achivo que se encuentra en nuestro servidor virtual)
* composer install
* luego de organizar el proyecto se debe descargar la base de datos en mysql adjuntada y agregarla a localhost.
