<?php

use Illuminate\Support\Facades\Route;

/* Rutas de Sales */
Route::get('/', 'SalesController@ListProducts')->name('Sales');
Route::post('Createsale','SalesController@CreateSales');
Route::post('editsale/{IdSales}','SalesController@EditSales');

/* Rutas de Products */
Route::get('/product', 'ProductsController@ListProducts')->name('Product');
Route::post('createproduct','ProductsController@CreateProducts');
Route::post('editproduct/{IdProduct}','ProductsController@EditProducts');
Route::post('inactivateproduct/{IdProduct}','ProductsController@InactivateProducts');

/* Rutas de categorias */
Route::get('/Categories', 'CategoriesController@ListProducts')->name('Categories');
Route::post('createcategory','CategoriesController@CreateCategories');
Route::post('editcategories/{IDCategory}','CategoriesController@EditCategories');
Route::post('inactivatecategory/{IDCategory}','CategoriesController@InactivateCategories');

/* Rutas de referencias */
Route::get('/References', 'ReferencesController@ListProducts')->name('References');
Route::post('createreferences','ReferencesController@CreateReferences');
Route::post('editreferences/{IdReference}','ReferencesController@EditReferences');
Route::post('inactivatereferences/{IdReference}','ReferencesController@InactivateReferences');
