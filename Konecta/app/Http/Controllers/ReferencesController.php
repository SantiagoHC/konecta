<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Products;
use App\References;
use App\Sales;

class ReferencesController extends Controller
{
    /** funcion para listar todos los datos reuqeridos en esta vista
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function ListProducts()
    {
        /* Consultamos todas las referencias activas en el sistema */
        $References = References::
        select(
        'IdReference',
        'Name',
        'Description',
        'Status'
        )
        ->where('Status', 1)
        ->orderBy('IdReference', 'desc')->get();

        return view('References', compact(['References']));
    }
    /** funcion para crear referencias
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function CreateReferences(Request $request)
    {
        /* Validamos que los datos requeridos para la inserccion existan en base de datos */
        $validate = \Validator::make($request->all(), [
            'Name'            => 'required'
        ]);
        if ($validate->fails()) {
            $info = $request->session()->flash('failed','Porfavor envia todos los datos requeridos');
            return redirect('References')->with('info', $info);
        } else {
            /* Obtenemos todos los datos enviados por el formulario */
            $data = $request->input();
			try{
                /* Validamos si la descripcion viene vacia y en caso tal guardala como null */
                if (!empty($data['Description'])) {
                    $Description = $data['Description'];
                }else {
                    $Description = NULL;
                }
                /* Almacenamos en bd los datos requeridos en Bd */
                $References = new References;
                $References->Name = $data['Name'];
                $References->Description = $Description;
                $References->Status = 1;
                /* Si guarda notificar que fue correcto el ingreso */
                if ($References->save()) {
                    $info = $request->session()->flash('successMsg','Se creo exitosamente la referencia '.$data['Name']);
                    return redirect('References')->with('info', $info);
                }else {
                    $info = $request->session()->flash('failed','Se pudo guardar la referencia '.$data['Name']);
                    return redirect('References')->with('info', $info);
                }
			}
			catch(Exception $e){
                $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
                return redirect('References')->with('info', $info);
			}
		} 
    }
    /** funcion para editar referencias
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function EditReferences($IdReference, Request $request)
    {
        /* Validamos que los datos requeridos para la inserccion existan en base de datos */
        $validate = \Validator::make($request->all(), [
            'Name'            => 'required'
        ]);
        if ($validate->fails()) {
            $info = $request->session()->flash('failed','Porfavor envia todos los datos requeridos');
            return redirect('References')->with('info', $info);
        } else {
            /* Obtenemos todos los datos enviados por el formulario */
            $data = $request->input();
			try{
                /* Validamos si la descripcion viene vaciay en caso tal guardala como null */
                if (!empty($data['Description'])) {
                    $Description = $data['Description'];
                }else {
                    $Description = NULL;
                }
                /* actualizamos todos los datos recibidos a la tabla de referencias */
                $References = References::where('IdReference',$IdReference)
                ->update(
                    array(
                        'Name' => $data['Name'],
                        'Description' => $Description
                    )
                );
                /* Si guarda notificar que fue correcta modificacion */
                if ($References) {
                    $info = $request->session()->flash('successMsg','Se edito exitosamente la referencia '.$data['Name']);
                    return redirect('References')->with('info', $info);
                }else {
                    $info = $request->session()->flash('failed','No se pudo editar la referencia '.$data['Name']);
                    return redirect('References')->with('info', $info);
                }
			}
			catch(Exception $e){
                $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
                return redirect('References')->with('info', $info);
			}
		} 
    }
    /** funcion para inactivar referencias
     * @author Santiago Hernandez <Santi-he-@hotmail.com>
     */
    public function InactivateReferences($IdReference, Request $request)
    {
        try{
            /* Actualizamos el estado de una referencia segun el id recibido */
            $Product = References::where('IdReference',$IdReference)
            ->update(
                array(
                    'Status' => 0,
                )
            );
            /* si actualiza correctamente notificar */
            if ($Product) {
                $info = $request->session()->flash('successMsg','Se elimino exitosamente la categoria');
                return redirect('References')->with('info', $info);
            }else {
                $info = $request->session()->flash('failed','No se pudo eliminar la categoria');
                    return redirect('References')->with('info', $info);
            }
        }
        catch(Exception $e){
            $info = $request->session()->flash('failed','Error En la consulta, porfavor comunicate con un administrador');
            return redirect('References')->with('info', $info);
        }
    }
    
}
